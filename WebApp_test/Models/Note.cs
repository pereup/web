﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp_test.Models
{

	/// <summary>
	/// Объект, который будет храниться в базе данных
	/// </summary>
	public class Note
	{
		[Key]
		public Guid _id { get; set; } // must have ... 30 minutes wasted...

		public Guid _idUser { get; set; } // id автора заметки

		public string NoteName { get; set; }	// Название заметки
		public string NoteValue { get; set; }	// Текст заметки
		public int Priority { get; set; }		// Приоритет "важности"
		public DateTime Deadline { get; set; }  // Время Deadline


			/* Редактирование заметок */
		private void ChangeName(string str) { this.NoteName = str; }
		private void ChangeValue(string str) { this.NoteValue = str; }
		private void ChangePriority(int priority) { this.Priority = priority; }
		private void ChangeDeadline(DateTime purpouse) { this.Deadline = purpouse; }

		//public Note()
		//{
		//	_id = Guid.NewGuid();
		//}
	}
}
