﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebApp_test.Models
{

	/// <summary>
	/// Чтобы взаимодействовать с базой данных через Entity Framework нам нужен контекст данных - класс, унаследованный от класса Microsoft.EntityFrameworkCore.DbContext.
	/// </summary>
	public class MobileContext : DbContext
	{
		public DbSet<Note> Notes { get; set; } // коллекция объектов, которая сопоставляется с определенной таблицей в базе данных
		public MobileContext(DbContextOptions<MobileContext> options)
			: base(options)
		{
		}
	}
}
