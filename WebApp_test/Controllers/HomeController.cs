﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp_test.Models;

namespace WebApp_test.Controllers
{
	public class HomeController : Controller
	{
		/*public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }*/

		private MobileContext db;

		public HomeController(MobileContext context)
		{
			db = context;
		}

		public IActionResult About()
		{
			return View(ViewData["About"]);
		}

		public IActionResult Contact()
		{
			return View(ViewData["Contact"]);
		}

		public async Task<IActionResult> Add() // Переход на AddNote
		{
			return View("AddNote", await db.Notes.ToListAsync());
		}

		[ActionName("Show")]
		public async Task<IActionResult> Show() // Переход на AddNote
		{
			return View("Show", await db.Notes.ToListAsync());
		}

		public async Task<IActionResult> Index()
		{
			return View(await db.Notes.ToListAsync());
		}

		public IActionResult Create()
		{
			return View();
		}

		public IActionResult DeleteShow()
		{
			return View("Delete");
		}

		[HttpPost]
		public async Task<IActionResult> Create(Note note)
		{
			db.Notes.Add(note);                 // получаем объекты из бд, создаваем из них список и передаваем в представление
			await db.SaveChangesAsync();        // формируем sql-выражение INSERT 
			return RedirectToAction("Index");   // выполняет выражение 
		}

		public async Task<IActionResult> Details(Guid? id)
		{
			if (id != null)
			{
				Note note = await db.Notes.FirstOrDefaultAsync(p => p._id == id);
				if (note != null)
					return View(note);
			}
			return NotFound();
		}

		[HttpGet]
		public async Task<IActionResult> Delete(Note note)
		{
			Note note1 = await db.Notes.FirstOrDefaultAsync(p => p.NoteName == note.NoteName);
			//Note note = await db.Notes.FirstOrDefaultAsync(p => p._id == id);
			if (note1 != null)
			{
				db.Notes.Remove(note1);
				await db.SaveChangesAsync();
				return RedirectToAction("Show");
			}

			return NotFound();
		}
	}
}
