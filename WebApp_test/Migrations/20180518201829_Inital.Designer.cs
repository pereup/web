﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using WebApp_test.Models;

namespace WebApp_test.Migrations
{
    [DbContext(typeof(MobileContext))]
    [Migration("20180518201829_Inital")]
    partial class Inital
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.3-rtm-10026")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("WebApp_test.Models.Note", b =>
                {
                    b.Property<Guid>("_id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Deadline");

                    b.Property<string>("NoteName");

                    b.Property<string>("NoteValue");

                    b.Property<int>("Priority");

                    b.HasKey("_id");

                    b.ToTable("Notes");
                });
#pragma warning restore 612, 618
        }
    }
}
